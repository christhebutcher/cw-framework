import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostComponent } from './post.component';
import { PostResolver } from './post.resolver';

const detailOptions = {
  component: PostComponent,
  resolve: {
    post: PostResolver,
  }
};

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PostComponent,
    resolve: { }
  },
  {
    path: ':id',
    ...detailOptions
  },
  {
    path: ':id/:title',
    ...detailOptions
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [PostResolver]
})
export class PostRoutingModule { }
