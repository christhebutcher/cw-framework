import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html'
})
export class PostComponent implements OnInit {
  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }
}
