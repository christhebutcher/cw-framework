import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PostResolver implements Resolve<Observable<any>> {

  constructor() {}

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params['id'];

    return of({});
  }
}
