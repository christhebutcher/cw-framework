import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      pathMatch: 'full',
      component: HomeComponent,
      resolve: { }
    }])
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
