import { NgModule, ChangeDetectorRef } from '@angular/core';
import { CommonModule, AsyncPipe } from '@angular/common';
import { ArraySortPipe } from './pipes/sort.pipe';
import { SafePipe } from './pipes/safe.pipe';
import { NavComponent } from './components/nav/nav.component';
import { HeroComponent } from './components/hero/hero.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DocPipe } from './pipes/doc.pipe';

const pipes = [
  ArraySortPipe,
  SafePipe,
  DocPipe,
];
const components = [
  HeroComponent,
  NavComponent,
];

const declarations = [...pipes, ...components];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations,
  providers: [AsyncPipe],
  exports: declarations
})
export class SharedModule { }
