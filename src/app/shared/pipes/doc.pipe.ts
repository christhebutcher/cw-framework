import { Pipe, PipeTransform } from '@angular/core';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { from, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'doc',
})
export class DocPipe implements PipeTransform {

  constructor(
    private db: AngularFirestore
    ) {}

  transform(value: any) {
    if (!value || !value.path) {
      return of(null);
    }
    return from(this.db.firestore
      .doc(value.path)
      .get()
    ).pipe(map(r => (r as any).exists ? (r as any).data() : null))
  }

}
