import { Utils } from './../../classes/utils.class';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
})
export class HeroComponent extends Utils implements OnInit {
  @Input() title: string;
  @Input() intro: string;
  @Input() img: string;
  @Input() video: string;
  @Output() loaded = new EventEmitter();
  imgLoaded = false;
  videoLoaded = false;

  constructor() {
    super();
  }

  ngOnInit() {

  }

  loadImg() { this.imgLoaded = true; }
  loadVideo() { this.videoLoaded = true; }
}
