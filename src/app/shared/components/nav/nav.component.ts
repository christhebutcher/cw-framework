import { Component, AfterViewInit, Input, ViewChild, OnChanges } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
})
export class NavComponent implements AfterViewInit, OnChanges {
  open = false;

  constructor() { }

  ngOnChanges() {

  }

  ngAfterViewInit() {

  }

  toggle() {
    this.open = !this.open;
    this.updateBody();
  }

  close() {
    this.open = false;
    this.updateBody();
  }

  updateBody() {
    if (typeof window !== 'undefined') {
      document.body.classList.toggle('nav--open', this.open);
      document.body.classList.toggle('nav--closed', !this.open);
    }
  }

}
