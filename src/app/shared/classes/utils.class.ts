export class Utils {

  static jsonp(url: string) {
    if (typeof window === 'object') {
      return new Promise((resolve, reject) => {
        const s4 = () =>
          Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        const callbackName = 'cb_' + s4();
        const script = document.createElement('script');
        window[callbackName] = data => {
          delete window[callbackName];
          resolve(data);
        };
        script.src = `${url}&callback=${callbackName}`;
        document.body.appendChild(script);
        (script as any).onload = script.remove();
      });
    }
  }

  isClient(): boolean {
    return typeof window !== 'undefined';
  }

  isService(): boolean {
    return typeof window === 'undefined';
  }

  isMobile(): boolean {
    return this.isClient() && /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
  }
}
