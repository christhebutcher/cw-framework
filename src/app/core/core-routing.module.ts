import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: '../features/home/home.module#HomeModule'
  },
  {
    path: 'post',
    loadChildren: '../features/post/post.module#PostModule'
  },
  {
    path: 'styleguide',
    loadChildren: '../features/styleguide/styleguide.module#StyleguideModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
