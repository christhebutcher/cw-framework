import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { CoreRoutingModule } from './core-routing.module';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreRoutingModule,
    BrowserModule
  ],
  declarations: [

  ],
  providers: [

  ],
  exports: [
    SharedModule,
    RouterModule
  ]
})
export class CoreModule { }
