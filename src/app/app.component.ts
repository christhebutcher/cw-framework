import { Component, OnInit } from '@angular/core';
import { Meta as Meta2, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'App';

  constructor(
    private metaService: Meta2,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.metaService.addTag({ name: 'application-name', content: this.title });
  }
}
